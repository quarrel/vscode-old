---
"$schema": https://raw.githubusercontent.com/martinring/tmlanguage/master/tmlanguage.json
name: Quarrel
patterns:
- include: "#comments"
- include: "#functions"
- include: "#assignments"
- include: "#keywords"
- include: "#strings"
- include: "#loose_containers"
- include: "#booleans"
- include: "#numbers"
- include: "#variables"
- include: "#contracts"
- include: "#operators"
- include: "#calls"
- include: "#modules"
repository:
  comments:
    patterns:
    - name: comment.block.quarrel
      begin: "^###"
      end: "^###"
    - name: comment.line.quarrel
      begin: "#(?![\\[_])"
      end: "\\n"
    - name: comment.bracketed.quarrel
      begin: "#\\["
      end: "\\]#"
  modules:
    patterns:
    - name: source.module.quarrel
      begin: "([\\[\\{]\\|)"
      beginCaptures:
        '1':
          name: punctuation.section.embedded.begin.module.quarrel
      end: "(\\|[\\}\\]])"
      endCaptures:
        '1':
          name: punctuation.section.embedded.end.module.quarrel
      patterns:
      - include: "$self"
  functions:
    patterns:
    - name: storage.type.function.quarrel
      match: "(?<=\\w|\\d|\\s|\\)|\\}|\\])[=~*]\\>(?=\\w|\\d|\\s|\\(|\\{|\\[)"
  assignments:
    patterns:
    - begin: "\\b([a-zA-Z_][a-zA-Z0-9_]*)(?=(?:\\\\[a-zA-Z_][a-zA-Z0-9_]*)+\\s*[:.\\\\]=)"
      end: "[:.|\\\\]="
      beginCaptures:
        '1':
          name: meta.variable.hash.parent.quarrel
      endCaptures:
        '0':
          name: keyword.operator.assignment.quarrel
      patterns:
      - include: "#slots"
    - match: "\\b([a-zA-Z_][a-zA-Z0-9_]*)\\s*([:.|\\\\]=)"
      captures:
        '1':
          name: variable.name.assignment.quarrel
        '2':
          name: keyword.operator.assignment.quarrel
    - match: "'([a-zA-Z_][a-zA-Z0-9_]*)'\\s*([:.|\\\\]=)"
      captures:
        '1':
          name: variable.name.assignment.quarrel
        '2':
          name: keyword.operator.assignment.quarrel
    - match: "(?<=\\]|\\})\\s*([:.|\\\\]=)"
      captures:
        '1':
          name: keyword.operator.assignment.quarrel
  slots:
    patterns:
    # - match: "(\\\\)([a-zA-Z_][a-zA-Z0-9_]*|\\d+)(?=\\s*[:.|?^/]=[^\\n]*[\\=\\*]\\>)"
    #   captures:
    #     '1':
    #       name: storage.modifier.slot.quarrel
    #     '2':
    #       name: entity.name.function.slot.quarrel
    - match: "(\\\\)([a-zA-Z_][a-zA-Z0-9_]*|\\d+)(?=\\s*[:.|\\\\]=)"
      captures:
        '1':
          name: storage.modifier.slot.quarrel
        '2':
          name: variable.name.assignment.slot.quarrel
    - match: "(\\\\)([a-zA-Z_][a-zA-Z0-9_]*|\\d+)(?=#)"
      captures:
        '1':
          name: storage.modifier.slot.quarrel
        '2':
          name: meta.variable.slot.quarrel
    - match: "(\\\\)([a-zA-Z_][a-zA-Z0-9_]*|\\d+)"
      captures:
        '1':
          name: storage.modifier.slot.quarrel
        '2':
          name: entity.name.method.slot.quarrel
  contracts:
    patterns:
    - match: "\\b([a-zA-Z_][a-zA-Z0-9_]*)\\s*([?/^|]\\=)"
      captures:
        '1':
          name: storage.type.contract.quarrel
        '2':
          name: keyword.operator.assignment.quarrel
    - name: storage.type.contract.quarrel
      match: "\\b([a-zA-Z_][a-zA-Z0-9_]*[?^])(?=\\s)"
    - name: storage.type.contract.autogen.quarrel
      match: "\\b([a-zA-Z_][a-zA-Z0-9_]*)(?=\\{)"
    - name: storage.type.contract.autogen.op.quarrel
      match: "(?<=\\})[?^]"
  keywords:
    patterns:
    - name: keyword.control.quarrel
      match: "(?<=\\w|\\d|\\s|\\)|\\}|\\])(?:&&|\\|\\||\\!\\!)(?=\\w|\\d|\\s|\\(|\\{|\\[)"
    - name: keyword.control.conditional.quarrel
      match: "[@~?!]@"
    - name: keyword.control.empirical.quarrel
      match: "[$~?!]\\$"
      # match: "(?<!\\w|\\d|_)[@~?]@(?=[^\\n]+\\:)"
  strings:
    name: string.quoted.double.quarrel
    begin: "\""
    end: "\""
    patterns:
    - name: constant.character.escape.quarrel
      match: "`."
    - include: "#interpolated_quarrel"
  loose_containers:
    name: variable.name.container.quarrel
    begin: "'"
    end: "'"
  interpolated_quarrel:
    patterns:
    - begin: "(\\{\\{)"
      beginCaptures:
        1:
          name: punctuation.section.embedded.begin.quarrel
      contentName: source.quarrel
      end: "(\\}\\})"
      endCaptures:
        1:
          name: punctuation.section.embedded.end.quarrel
        # '1':
        #   name: source.quarrel
      name: meta.embedded.line.quarrel
      patterns:
      - include: "#nest_curly_and_self"
      - include: "$self"
      repository:
        nest_curly_and_self:
          patterns:
          - begin: "\\{"
            captures:
              '0':
                name: punctuation.section.scope.quarrel
            end: "\\}"
            patterns:
            - include: "#nest_curly_and_self"
          - include: "$self"
    - begin: "#\\["
      beginCaptures:
        0:
          name: punctuation.section.embedded.begin.quarrel
      contentName: source.quarrel
      end: "(\\])"
      endCaptures:
        1:
          name: punctuation.section.embedded.end.quarrel
        # '1':
        #   name: source.quarrel
      name: meta.embedded.line.quarrel
      patterns:
      - include: "#nest_bracket_and_self"
      - include: "$self"
      repository:
        nest_bracket_and_self:
          patterns:
          - begin: "\\["
            captures:
              '0':
                name: punctuation.section.scope.quarrel
            end: "\\]"
            patterns:
            - include: "#nest_bracket_and_self"
          - include: "$self"
  booleans:
    patterns:
    - name: constant.language.boolean.quarrel
      match: "\\b(?:yes|no|true|false)\\b"
  numbers:
    patterns:
    - name: constant.numeric.decimal.quarrel
      match: "\\b\\d[\\d_]*(?:\\.[\\d_]+)?(?:[eE][\\+\\-]?[\\d_]+)?\\b"
  variables:
    patterns:
    - name: entity.other.attribute-name.scss.wildcard.quarrel
      match: "(?<!\\w|\\:)(?:\\?)?_(?!\\:|\\w)"
    - name: variable.other.record.attribute.quarrel
      match: >
        (?x)
          (?<= < )( :[_?] | [_?]: )(?= > )
    - name: entity.name.type.type-parameter.record.key.quarrel
      match: "(?<!\\w|\\]|\\>)\\\\\\w+[?^]?(?!\\\\)"
    # - match: "(?<=\\p{Space}|\\p{Punct})(\\p{Alpha}(?:\\p{Word}|_)*\\??[.:])"
    - match: >
        (?x)
          (?:^\p{Space}*)
          (\p{Alpha}(?:\p{Word}|_)*\??[.:])
          (?=\p{Space})
          |
          (?:(?<=\p{Alpha})\p{Space}+)
          (\p{Alpha}(?:\p{Word}|_)*\??[.:])
          (?=\p{Space})
          |
          (?:(?<=\p{Punct}|[=<])(?<![@$])\p{Space}*)
          (\p{Alpha}(?:\p{Word}|_)*\??[.:])
          (?=\p{Space})
          |
          (?:(?<=\p{Punct}|[=<])(?<![@$])\p{Space}*)
          (\??[.:]\p{Alpha}(?:\p{Word}|_)*)
          (?:\p{Space}*(?=\p{Punct}|\n|>|=>))
      captures:
        1: {name: entity.name.type.type-parameter.argument.named.1.quarrel}
        2: {name: entity.name.type.type-parameter.argument.named.2.quarrel}
        3: {name: entity.name.type.type-parameter.argument.named.3.quarrel}
        4: {name: entity.name.type.type-parameter.argument.named.4.quarrel}
  operators:
    patterns:
    - name: keyword.operator.quarrel
      match: "&&&|\\|\\|\\||\\^\\^\\^|!!!|==|<>|\\>=|<=|>{3}|>{2}(?!-)|<<<?|(?<!\\>)\\+{1,3}(?!\\>)|(?<!\\>)\\-{1,3}(?!\\>)|\\/{1,3}(?!=)|%{1,2}|\\*(?=\\s)|\\*{2,3}|\\.{2,3}"
    - name: variable.language.function.pipe.quarrel
      match: \|(?:[-=]\|)?|\/=\/
    - name: keyword.control.transform.quarrel
      match: "<\\+|\\+>|\\|?\\->|<-\\|?|\\|{1,3}>|<\\|{1,3}|>>?-|-<?<|<\\$|\\$>|~=|~(?!@)|\\*(?!\\s)"
    - name: keyword.operator.list.quarrel
      match: ":(?=_>)|(?<=<_):|<:(?!_)|(?<!_):>|[>!]:|:[<!:]|(?<=\\[|<)\\?(?=\\]|>)"
  calls:
    patterns:
    - match: >
        (?x)
        \b([a-zA-Z_][a-zA-Z0-9_]*)(?=\[|\s+(?=\w|\d|"|\(|\{|\\|\[))
        |
        ('\w[\w\s]*')(?=\[|\s+(?=\w|\d|"|\(|\{|\\|\[))
      captures:
        1: {name: entity.name.function.quarrel}
        2: {name: entity.name.function.quoted.quarrel}
    - match: >
        (?x)
        \b([a-zA-Z_][a-zA-Z0-9_]*)(?=\![^\!\:])
        |
        ('\w[\w\s]*')(?=\![^\!\:])
      captures:
        1: {name: entity.name.function.bang.quarrel}
        2: {name: entity.name.function.quoted.bang.quarrel}
scopeName: source.quarrel
